--Basic electron calculator app example for MACOS ONLY--
You can deploy on other platforms cause duh, its electron! but you need to change path parameters and stuff. WARNING: this is just and example to see how to bind with Vue.js and how to make a simple electron app so the calculator has tons of bugs.

-Summary-
Simple example to go by, a simple calculator made in electron. html and css resolved with Bootstrap and javascript data binding done with Vue.js.

-Developer-
Leandro Kim.

-Launch-
npm install
npm start

-Build-
[npm install] //if you did not install before to test
npm run build

This will delete any previous build, build the app again, copy the icon file to the app directory, hide the source file inside with ASAR, and delete the source file. Only tested in macOS so if you want to build on windows, you should probably change the paths.
