const math = require('mathjs');

var app = new Vue({
	el: '#app',
	data: {
		message: '',
		toReset: false
	},
	methods: {
		type: function(char) {
			if (this.toReset) {
				this.clear();
				this.toReset = false;
			}
			this.message += char;
		},
		equal: function() {
			try {
				this.message = math.eval(this.message);
				if(this.message == "0") this.toReset = true;
			} catch (err) {
				this.message = "SYNTAX ERROR";
				this.toReset = true;
			}
		},
		clear: function() {
			this.message = "";
		}
	}
});

window.$ = window.jQuery = require('jquery');
window.Bootstrap = require('bootstrap');
